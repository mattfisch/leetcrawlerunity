#Project: L33tCrawler
##Name: TBD
Name is currently the projects codename, leetcrawler.

##Members
- Fischbacher Matthias
- W�hrer Gerhard

Matthias made the sprites for enemies und dungeon, the eventbus and the player animations.

Gerhard made the sprites for player, ui and menus.

Gamelogic was done pair-programming.

##Game Type
2D Rogue like dungeon crawling

##Used Engine
Unity Engine

##Gameplay
The player can setup a character with some predefined stuff. The game is 2D and the player can move from left to right, not beeing able to move back. He has to fight enemies and loot stuff until he dies.

The goal is to loot as much as possible, lighting up blown out torches to increase the light level and kill enemies, all this things contribute to a total score. Goal is to reach a score as high as possible, while not dying.

##Features
Light gets dimmer and darker as he dwells deeper into the depths of the dungeon.
Loot (Score)
Enemies

##Controls
On screen gamepad like touch controls

##Graphics
Pixelart

![](Screenshots/ingame_pre_alpha_01.png)
Actual pre-alpha ingame footage