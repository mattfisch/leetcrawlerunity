<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/vortech/workspace/git/leetcrawlerunity/Sprites/Units/UnitTextureMap.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../LeetCrawler/Assets/Sprites/Units/Enemies.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">CropKeepPos</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Blobrat/Attack/Blobrat_attack_01.png</key>
            <key type="filename">Blobrat/Death/Blobrat_death_01.png</key>
            <key type="filename">Blobrat/Flying/Blobrat_flying_01.png</key>
            <key type="filename">Blobrat/Hit/Blobrat_hit_01.png</key>
            <key type="filename">Blobrat/Idle/Blobrat_idle_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Attack/Blobrat_attack_01_n.png</key>
            <key type="filename">Blobrat/Death/Blobrat_death_01_n.png</key>
            <key type="filename">Blobrat/Flying/Blobrat_flying_01_n.png</key>
            <key type="filename">Blobrat/Hit/Blobrat_hit_01_n.png</key>
            <key type="filename">Blobrat/Idle/Blobrat_idle_01_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Attack/Blobrat_attack_02.png</key>
            <key type="filename">Blobrat/Attack/Blobrat_attack_03.png</key>
            <key type="filename">Blobrat/Death/Blobrat_death_02.png</key>
            <key type="filename">Blobrat/Death/Blobrat_death_03.png</key>
            <key type="filename">Blobrat/Hit/Blobrat_hit_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Attack/Blobrat_attack_02_n.png</key>
            <key type="filename">Blobrat/Attack/Blobrat_attack_03_n.png</key>
            <key type="filename">Blobrat/Death/Blobrat_death_02_n.png</key>
            <key type="filename">Blobrat/Death/Blobrat_death_03_n.png</key>
            <key type="filename">Blobrat/Hit/Blobrat_hit_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Death/Blobrat_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,11,11</rect>
                <key>scale9Paddings</key>
                <rect>5,6,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Death/Blobrat_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,11,11</rect>
                <key>scale9Paddings</key>
                <rect>5,6,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Death/Blobrat_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Death/Blobrat_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Death/Blobrat_death_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,2,3,3</rect>
                <key>scale9Paddings</key>
                <rect>1,2,3,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Death/Blobrat_death_06_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,2,3,3</rect>
                <key>scale9Paddings</key>
                <rect>1,2,3,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Flying/Blobrat_flying_02.png</key>
            <key type="filename">Blobrat/Flying/Blobrat_flying_03.png</key>
            <key type="filename">Blobrat/Idle/Blobrat_idle_02.png</key>
            <key type="filename">Blobrat/Idle/Blobrat_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,15,14</rect>
                <key>scale9Paddings</key>
                <rect>8,7,15,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Flying/Blobrat_flying_02_n.png</key>
            <key type="filename">Blobrat/Flying/Blobrat_flying_03_n.png</key>
            <key type="filename">Blobrat/Idle/Blobrat_idle_02_n.png</key>
            <key type="filename">Blobrat/Idle/Blobrat_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,15,14</rect>
                <key>scale9Paddings</key>
                <rect>8,7,15,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Hit/Blobrat_hit_02.png</key>
            <key type="filename">Knight/Idle/Knight_idle_02.png</key>
            <key type="filename">Knight/Idle/Knight_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Blobrat/Hit/Blobrat_hit_02_n.png</key>
            <key type="filename">Knight/Idle/Knight_idle_02_n.png</key>
            <key type="filename">Knight/Idle/Knight_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_01.png</key>
            <key type="filename">Fireelement/Death/Fireelement_death_01.png</key>
            <key type="filename">Fireelement/Hit/Fireelement_hit_01.png</key>
            <key type="filename">Fireelement/Hit/Fireelement_hit_02.png</key>
            <key type="filename">Fireelement/Hit/Fireelement_hit_03.png</key>
            <key type="filename">Fireelement/Idle/Fireelement_idle_01.png</key>
            <key type="filename">Fireelement/Walking/Fireelement_walking_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,10,17</rect>
                <key>scale9Paddings</key>
                <rect>5,9,10,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_01_n.png</key>
            <key type="filename">Fireelement/Death/Fireelement_death_01_n.png</key>
            <key type="filename">Fireelement/Hit/Fireelement_hit_01_n.png</key>
            <key type="filename">Fireelement/Hit/Fireelement_hit_02_n.png</key>
            <key type="filename">Fireelement/Hit/Fireelement_hit_03_n.png</key>
            <key type="filename">Fireelement/Idle/Fireelement_idle_01_n.png</key>
            <key type="filename">Fireelement/Walking/Fireelement_walking_01_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,10,17</rect>
                <key>scale9Paddings</key>
                <rect>5,9,10,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,11,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,11,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,11,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,11,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,11,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,11,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Attack/Fireelement_attack_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,11,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,11,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,15</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,15</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,11,14</rect>
                <key>scale9Paddings</key>
                <rect>6,7,11,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,11,14</rect>
                <key>scale9Paddings</key>
                <rect>6,7,11,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_05.png</key>
            <key type="filename">Rat/Attack/Rat_attack_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,13,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_05_n.png</key>
            <key type="filename">Rat/Attack/Rat_attack_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,13,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,3,13,6</rect>
                <key>scale9Paddings</key>
                <rect>6,3,13,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_06_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,3,13,6</rect>
                <key>scale9Paddings</key>
                <rect>6,3,13,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,2,7,3</rect>
                <key>scale9Paddings</key>
                <rect>3,2,7,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Death/Fireelement_death_07_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,2,7,3</rect>
                <key>scale9Paddings</key>
                <rect>3,2,7,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Idle/Fireelement_idle_02.png</key>
            <key type="filename">Fireelement/Idle/Fireelement_idle_04.png</key>
            <key type="filename">Fireelement/Walking/Fireelement_walking_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Idle/Fireelement_idle_02_n.png</key>
            <key type="filename">Fireelement/Idle/Fireelement_idle_04_n.png</key>
            <key type="filename">Fireelement/Walking/Fireelement_walking_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Idle/Fireelement_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,16</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Idle/Fireelement_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,16</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Walking/Fireelement_walking_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,13,17</rect>
                <key>scale9Paddings</key>
                <rect>6,8,13,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Walking/Fireelement_walking_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,13,17</rect>
                <key>scale9Paddings</key>
                <rect>6,8,13,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Walking/Fireelement_walking_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Walking/Fireelement_walking_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Walking/Fireelement_walking_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Fireelement/Walking/Fireelement_walking_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Attack/Ghost_attack_01.png</key>
            <key type="filename">Ghost/Attack/Ghost_attack_03.png</key>
            <key type="filename">Ghost/Death/Ghost_death_01.png</key>
            <key type="filename">Ghost/Flying/Ghost_flying_01.png</key>
            <key type="filename">Ghost/Flying/Ghost_flying_02.png</key>
            <key type="filename">Ghost/Flying/Ghost_flying_04.png</key>
            <key type="filename">Ghost/Hit/Ghost_hit_01.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_01.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_02.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,18</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Attack/Ghost_attack_01_n.png</key>
            <key type="filename">Ghost/Attack/Ghost_attack_03_n.png</key>
            <key type="filename">Ghost/Death/Ghost_death_01_n.png</key>
            <key type="filename">Ghost/Flying/Ghost_flying_01_n.png</key>
            <key type="filename">Ghost/Flying/Ghost_flying_02_n.png</key>
            <key type="filename">Ghost/Flying/Ghost_flying_04_n.png</key>
            <key type="filename">Ghost/Hit/Ghost_hit_01_n.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_01_n.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_02_n.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,18</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Attack/Ghost_attack_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,9,11,18</rect>
                <key>scale9Paddings</key>
                <rect>6,9,11,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Attack/Ghost_attack_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,9,11,18</rect>
                <key>scale9Paddings</key>
                <rect>6,9,11,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,9,16</rect>
                <key>scale9Paddings</key>
                <rect>5,8,9,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,9,16</rect>
                <key>scale9Paddings</key>
                <rect>5,8,9,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,6,7,13</rect>
                <key>scale9Paddings</key>
                <rect>3,6,7,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,6,7,13</rect>
                <key>scale9Paddings</key>
                <rect>3,6,7,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,5,5,10</rect>
                <key>scale9Paddings</key>
                <rect>2,5,5,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,5,5,10</rect>
                <key>scale9Paddings</key>
                <rect>2,5,5,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,3,4,5</rect>
                <key>scale9Paddings</key>
                <rect>2,3,4,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Death/Ghost_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,3,4,5</rect>
                <key>scale9Paddings</key>
                <rect>2,3,4,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Flying/Ghost_flying_03.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,17</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Flying/Ghost_flying_03_n.png</key>
            <key type="filename">Ghost/Idle/Ghost_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,17</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Hit/Ghost_hit_02.png</key>
            <key type="filename">Ghost/Hit/Ghost_hit_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,10,18</rect>
                <key>scale9Paddings</key>
                <rect>5,9,10,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Ghost/Hit/Ghost_hit_02_n.png</key>
            <key type="filename">Ghost/Hit/Ghost_hit_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,10,18</rect>
                <key>scale9Paddings</key>
                <rect>5,9,10,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Attack/Imp_attack_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,13,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,13,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Attack/Imp_attack_01_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,13,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,13,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Attack/Imp_attack_02.png</key>
            <key type="filename">Imp/Attack/Imp_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,13,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,13,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Attack/Imp_attack_02_n.png</key>
            <key type="filename">Imp/Attack/Imp_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,13,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,13,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Attack/Imp_attack_04.png</key>
            <key type="filename">Imp/Death/Imp_death_01.png</key>
            <key type="filename">Imp/Hit/Imp_hit_01.png</key>
            <key type="filename">Imp/Hit/Imp_hit_03.png</key>
            <key type="filename">Imp/Idle/Imp_idle_01.png</key>
            <key type="filename">Imp/Walking/Imp_walking_01.png</key>
            <key type="filename">Imp/Walking/Imp_walking_02.png</key>
            <key type="filename">Imp/Walking/Imp_walking_04.png</key>
            <key type="filename">Imp/Walking/Imp_walking_05.png</key>
            <key type="filename">Imp/Walking/Imp_walking_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Attack/Imp_attack_04_n.png</key>
            <key type="filename">Imp/Death/Imp_death_01_n.png</key>
            <key type="filename">Imp/Hit/Imp_hit_01_n.png</key>
            <key type="filename">Imp/Hit/Imp_hit_03_n.png</key>
            <key type="filename">Imp/Idle/Imp_idle_01_n.png</key>
            <key type="filename">Imp/Walking/Imp_walking_01_n.png</key>
            <key type="filename">Imp/Walking/Imp_walking_02_n.png</key>
            <key type="filename">Imp/Walking/Imp_walking_04_n.png</key>
            <key type="filename">Imp/Walking/Imp_walking_05_n.png</key>
            <key type="filename">Imp/Walking/Imp_walking_06_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,12,13</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,12,13</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,14,12</rect>
                <key>scale9Paddings</key>
                <rect>7,6,14,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,14,12</rect>
                <key>scale9Paddings</key>
                <rect>7,6,14,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,15,8</rect>
                <key>scale9Paddings</key>
                <rect>7,4,15,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,15,8</rect>
                <key>scale9Paddings</key>
                <rect>7,4,15,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,2,10,5</rect>
                <key>scale9Paddings</key>
                <rect>5,2,10,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,2,10,5</rect>
                <key>scale9Paddings</key>
                <rect>5,2,10,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,5,5</rect>
                <key>scale9Paddings</key>
                <rect>2,2,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Death/Imp_death_06_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,5,5</rect>
                <key>scale9Paddings</key>
                <rect>2,2,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Hit/Imp_hit_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,11,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,11,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Hit/Imp_hit_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,11,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,11,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Idle/Imp_idle_02.png</key>
            <key type="filename">Imp/Idle/Imp_idle_04.png</key>
            <key type="filename">Imp/Walking/Imp_walking_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,12,15</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Idle/Imp_idle_02_n.png</key>
            <key type="filename">Imp/Idle/Imp_idle_04_n.png</key>
            <key type="filename">Imp/Walking/Imp_walking_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,12,15</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Idle/Imp_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,12,14</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Imp/Idle/Imp_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,12,14</rect>
                <key>scale9Paddings</key>
                <rect>6,7,12,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Attack/Knight_attack_01.png</key>
            <key type="filename">Knight/Death/Knight_death_01.png</key>
            <key type="filename">Knight/Hit/Knight_hit_01.png</key>
            <key type="filename">Knight/Hit/Knight_hit_02.png</key>
            <key type="filename">Knight/Hit/Knight_hit_03.png</key>
            <key type="filename">Knight/Idle/Knight_idle_01.png</key>
            <key type="filename">Knight/Idle/Knight_idle_04.png</key>
            <key type="filename">Knight/Walking/Knight_walking_01.png</key>
            <key type="filename">Knight/Walking/Knight_walking_02.png</key>
            <key type="filename">Knight/Walking/Knight_walking_03.png</key>
            <key type="filename">Knight/Walking/Knight_walking_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,15,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,15,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Attack/Knight_attack_01_n.png</key>
            <key type="filename">Knight/Death/Knight_death_01_n.png</key>
            <key type="filename">Knight/Hit/Knight_hit_01_n.png</key>
            <key type="filename">Knight/Hit/Knight_hit_02_n.png</key>
            <key type="filename">Knight/Hit/Knight_hit_03_n.png</key>
            <key type="filename">Knight/Idle/Knight_idle_01_n.png</key>
            <key type="filename">Knight/Idle/Knight_idle_04_n.png</key>
            <key type="filename">Knight/Walking/Knight_walking_01_n.png</key>
            <key type="filename">Knight/Walking/Knight_walking_02_n.png</key>
            <key type="filename">Knight/Walking/Knight_walking_03_n.png</key>
            <key type="filename">Knight/Walking/Knight_walking_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,15,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,15,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Attack/Knight_attack_02.png</key>
            <key type="filename">Knight/Attack/Knight_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,17,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,17,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Attack/Knight_attack_02_n.png</key>
            <key type="filename">Knight/Attack/Knight_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,17,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,17,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,15,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,15,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,15,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,15,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,15,13</rect>
                <key>scale9Paddings</key>
                <rect>7,6,15,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,15,13</rect>
                <key>scale9Paddings</key>
                <rect>7,6,15,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,15,10</rect>
                <key>scale9Paddings</key>
                <rect>7,5,15,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,15,10</rect>
                <key>scale9Paddings</key>
                <rect>7,5,15,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_05.png</key>
            <key type="filename">Knight/Death/Knight_death_06.png</key>
            <key type="filename">Knight/Death/Knight_death_07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,15,6</rect>
                <key>scale9Paddings</key>
                <rect>7,3,15,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Knight/Death/Knight_death_05_n.png</key>
            <key type="filename">Knight/Death/Knight_death_06_n.png</key>
            <key type="filename">Knight/Death/Knight_death_07_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,15,6</rect>
                <key>scale9Paddings</key>
                <rect>7,3,15,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Attack/Paladin_attack_01.png</key>
            <key type="filename">Paladin/Block/Paladin_block_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,21,22</rect>
                <key>scale9Paddings</key>
                <rect>11,11,21,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Attack/Paladin_attack_02.png</key>
            <key type="filename">Paladin/Block/Paladin_block_02.png</key>
            <key type="filename">Paladin/Block/Paladin_block_03.png</key>
            <key type="filename">Paladin/Block/Paladin_block_04.png</key>
            <key type="filename">Paladin/Death/Paladin_death_01.png</key>
            <key type="filename">Paladin/Hit/Paladin_hit_01.png</key>
            <key type="filename">Paladin/Hit/Paladin_hit_02.png</key>
            <key type="filename">Paladin/Hit/Paladin_hit_03.png</key>
            <key type="filename">Paladin/Idle/Paladin_idle_01.png</key>
            <key type="filename">Paladin/Idle/Paladin_idle_04.png</key>
            <key type="filename">Paladin/Walking/Paladin_walking_01.png</key>
            <key type="filename">Paladin/Walking/Paladin_walking_02.png</key>
            <key type="filename">Paladin/Walking/Paladin_walking_03.png</key>
            <key type="filename">Paladin/Walking/Paladin_walking_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,20,22</rect>
                <key>scale9Paddings</key>
                <rect>10,11,20,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Attack/Paladin_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,11,17,22</rect>
                <key>scale9Paddings</key>
                <rect>9,11,17,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Attack/Paladin_attack_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,15,28,30</rect>
                <key>scale9Paddings</key>
                <rect>14,15,28,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Attack/Paladin_attack_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,15,38,30</rect>
                <key>scale9Paddings</key>
                <rect>19,15,38,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Death/Paladin_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Death/Paladin_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,9,20,18</rect>
                <key>scale9Paddings</key>
                <rect>10,9,20,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Death/Paladin_death_04.png</key>
            <key type="filename">Slug/Death/Slug_death_02_n.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_03_n.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,13</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Death/Paladin_death_05.png</key>
            <key type="filename">Paladin/Death/Paladin_death_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,4,20,9</rect>
                <key>scale9Paddings</key>
                <rect>10,4,20,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Death/Paladin_death_07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,4,17,8</rect>
                <key>scale9Paddings</key>
                <rect>9,4,17,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin/Idle/Paladin_idle_02.png</key>
            <key type="filename">Paladin/Idle/Paladin_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,20,21</rect>
                <key>scale9Paddings</key>
                <rect>10,11,20,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin_attack.psd</key>
            <key type="filename">fire fly01.png</key>
            <key type="filename">fire fly01_n.png</key>
            <key type="filename">fire fly02.png</key>
            <key type="filename">fire fly02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin_attack_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin_attack_02.png</key>
            <key type="filename">Paladin_attack_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,12,23,24</rect>
                <key>scale9Paddings</key>
                <rect>11,12,23,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,12,19,24</rect>
                <key>scale9Paddings</key>
                <rect>10,12,19,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Paladin_attack_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,12,31,24</rect>
                <key>scale9Paddings</key>
                <rect>15,12,31,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Attack/Rat_attack_01.png</key>
            <key type="filename">Rat/Death/Rat_death_01.png</key>
            <key type="filename">Rat/Hit/Rat_hit_01.png</key>
            <key type="filename">Rat/Hit/Rat_hit_02.png</key>
            <key type="filename">Rat/Idle/Rat_idle_01.png</key>
            <key type="filename">Rat/Walking/Rat_walking_01.png</key>
            <key type="filename">Rat/Walking/Rat_walking_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,12,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,12,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Attack/Rat_attack_01_n.png</key>
            <key type="filename">Rat/Death/Rat_death_01_n.png</key>
            <key type="filename">Rat/Hit/Rat_hit_01_n.png</key>
            <key type="filename">Rat/Hit/Rat_hit_02_n.png</key>
            <key type="filename">Rat/Idle/Rat_idle_01_n.png</key>
            <key type="filename">Rat/Walking/Rat_walking_01_n.png</key>
            <key type="filename">Rat/Walking/Rat_walking_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,12,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,12,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Attack/Rat_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,13,9</rect>
                <key>scale9Paddings</key>
                <rect>7,5,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Attack/Rat_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,13,9</rect>
                <key>scale9Paddings</key>
                <rect>7,5,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,12,7</rect>
                <key>scale9Paddings</key>
                <rect>6,4,12,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,12,7</rect>
                <key>scale9Paddings</key>
                <rect>6,4,12,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,3,9,5</rect>
                <key>scale9Paddings</key>
                <rect>5,3,9,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,3,9,5</rect>
                <key>scale9Paddings</key>
                <rect>5,3,9,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,2,7,4</rect>
                <key>scale9Paddings</key>
                <rect>3,2,7,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,2,7,4</rect>
                <key>scale9Paddings</key>
                <rect>3,2,7,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_05.png</key>
            <key type="filename">Wolf/Death/Wolf_death_08.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,1,5,2</rect>
                <key>scale9Paddings</key>
                <rect>2,1,5,2</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Death/Rat_death_05_n.png</key>
            <key type="filename">Wolf/Death/Wolf_death_08_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,1,5,2</rect>
                <key>scale9Paddings</key>
                <rect>2,1,5,2</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Hit/Rat_hit_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,11,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,11,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Hit/Rat_hit_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,11,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,11,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Idle/Rat_idle_02.png</key>
            <key type="filename">Rat/Idle/Rat_idle_04.png</key>
            <key type="filename">Rat/Walking/Rat_walking_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,12,9</rect>
                <key>scale9Paddings</key>
                <rect>6,4,12,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Idle/Rat_idle_02_n.png</key>
            <key type="filename">Rat/Idle/Rat_idle_04_n.png</key>
            <key type="filename">Rat/Walking/Rat_walking_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,12,9</rect>
                <key>scale9Paddings</key>
                <rect>6,4,12,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Idle/Rat_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,12,8</rect>
                <key>scale9Paddings</key>
                <rect>6,4,12,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Rat/Idle/Rat_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,12,8</rect>
                <key>scale9Paddings</key>
                <rect>6,4,12,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_01.png</key>
            <key type="filename">Reaper/Death/Reaper_death_01.png</key>
            <key type="filename">Reaper/Hit/Reaper_hit_01.png</key>
            <key type="filename">Reaper/Hit/Reaper_hit_02.png</key>
            <key type="filename">Reaper/Hit/Reaper_hit_03.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_01.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_01.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_02.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,10,12,19</rect>
                <key>scale9Paddings</key>
                <rect>6,10,12,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_01_n.png</key>
            <key type="filename">Reaper/Death/Reaper_death_01_n.png</key>
            <key type="filename">Reaper/Hit/Reaper_hit_01_n.png</key>
            <key type="filename">Reaper/Hit/Reaper_hit_02_n.png</key>
            <key type="filename">Reaper/Hit/Reaper_hit_03_n.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_01_n.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_01_n.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_02_n.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,10,12,19</rect>
                <key>scale9Paddings</key>
                <rect>6,10,12,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,10,13,21</rect>
                <key>scale9Paddings</key>
                <rect>7,10,13,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,10,13,21</rect>
                <key>scale9Paddings</key>
                <rect>7,10,13,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,19,21</rect>
                <key>scale9Paddings</key>
                <rect>10,10,19,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,19,21</rect>
                <key>scale9Paddings</key>
                <rect>10,10,19,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,9,22,17</rect>
                <key>scale9Paddings</key>
                <rect>11,9,22,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,9,22,17</rect>
                <key>scale9Paddings</key>
                <rect>11,9,22,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_05.png</key>
            <key type="filename">Slug/Attack/Slug_attack_01.png</key>
            <key type="filename">Slug/Attack/Slug_attack_02.png</key>
            <key type="filename">Slug/Attack/Slug_attack_04.png</key>
            <key type="filename">Slug/Death/Slug_death_01.png</key>
            <key type="filename">Slug/Hit/Slug_hit_01.png</key>
            <key type="filename">Slug/Idle/Slug_idle_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,20,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,20,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Attack/Reaper_attack_05_n.png</key>
            <key type="filename">Slug/Attack/Slug_attack_01_n.png</key>
            <key type="filename">Slug/Attack/Slug_attack_02_n.png</key>
            <key type="filename">Slug/Attack/Slug_attack_04_n.png</key>
            <key type="filename">Slug/Death/Slug_death_01_n.png</key>
            <key type="filename">Slug/Hit/Slug_hit_01_n.png</key>
            <key type="filename">Slug/Idle/Slug_idle_01_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,20,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,20,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_02.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_02.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_03.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_04.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,9,12,19</rect>
                <key>scale9Paddings</key>
                <rect>6,9,12,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_02_n.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_02_n.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_03_n.png</key>
            <key type="filename">Reaper/Idle/Reaper_idle_04_n.png</key>
            <key type="filename">Reaper/Walking/Reaper_walking_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,9,12,19</rect>
                <key>scale9Paddings</key>
                <rect>6,9,12,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,13,19</rect>
                <key>scale9Paddings</key>
                <rect>7,9,13,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,13,19</rect>
                <key>scale9Paddings</key>
                <rect>7,9,13,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_04.png</key>
            <key type="filename">Reaper/Death/Reaper_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,14,19</rect>
                <key>scale9Paddings</key>
                <rect>7,9,14,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_04_n.png</key>
            <key type="filename">Reaper/Death/Reaper_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,14,19</rect>
                <key>scale9Paddings</key>
                <rect>7,9,14,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,19</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_06_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,19</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,9,8,19</rect>
                <key>scale9Paddings</key>
                <rect>4,9,8,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Reaper/Death/Reaper_death_07_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,9,8,19</rect>
                <key>scale9Paddings</key>
                <rect>4,9,8,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Attack/Slug_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,21,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,21,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Attack/Slug_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,21,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,21,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_02.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_03.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,13</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,6,19,12</rect>
                <key>scale9Paddings</key>
                <rect>9,6,19,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,6,19,12</rect>
                <key>scale9Paddings</key>
                <rect>9,6,19,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,4,16,9</rect>
                <key>scale9Paddings</key>
                <rect>8,4,16,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,4,16,9</rect>
                <key>scale9Paddings</key>
                <rect>8,4,16,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,3,11,6</rect>
                <key>scale9Paddings</key>
                <rect>6,3,11,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,3,11,6</rect>
                <key>scale9Paddings</key>
                <rect>6,3,11,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,7,4</rect>
                <key>scale9Paddings</key>
                <rect>4,2,7,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Death/Slug_death_06_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,7,4</rect>
                <key>scale9Paddings</key>
                <rect>4,2,7,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Hit/Slug_hit_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,19,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,19,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Hit/Slug_hit_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,19,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,19,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Hit/Slug_hit_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,8,19,15</rect>
                <key>scale9Paddings</key>
                <rect>9,8,19,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Hit/Slug_hit_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,8,19,15</rect>
                <key>scale9Paddings</key>
                <rect>9,8,19,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Idle/Slug_idle_02.png</key>
            <key type="filename">Slug/Walking/Slug_walking_01.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,14</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Idle/Slug_idle_02_n.png</key>
            <key type="filename">Slug/Walking/Slug_walking_01_n.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,14</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Idle/Slug_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,15</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Idle/Slug_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,20,15</rect>
                <key>scale9Paddings</key>
                <rect>10,7,20,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Walking/Slug_walking_02.png</key>
            <key type="filename">Slug/Walking/Slug_walking_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,6,21,13</rect>
                <key>scale9Paddings</key>
                <rect>10,6,21,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Slug/Walking/Slug_walking_02_n.png</key>
            <key type="filename">Slug/Walking/Slug_walking_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,6,21,13</rect>
                <key>scale9Paddings</key>
                <rect>10,6,21,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Attack/Spirit_attack_01.png</key>
            <key type="filename">Spirit/Attack/Spirit_attack_02.png</key>
            <key type="filename">Spirit/Attack/Spirit_attack_03.png</key>
            <key type="filename">Spirit/Death/Spirit_death_01.png</key>
            <key type="filename">Spirit/Hit/Spirit_hit_01.png</key>
            <key type="filename">Spirit/Idle/Spirit_idle_01.png</key>
            <key type="filename">Spirit/Walking/Spirit_walking_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Attack/Spirit_attack_01_n.png</key>
            <key type="filename">Spirit/Attack/Spirit_attack_02_n.png</key>
            <key type="filename">Spirit/Attack/Spirit_attack_03_n.png</key>
            <key type="filename">Spirit/Death/Spirit_death_01_n.png</key>
            <key type="filename">Spirit/Hit/Spirit_hit_01_n.png</key>
            <key type="filename">Spirit/Idle/Spirit_idle_01_n.png</key>
            <key type="filename">Spirit/Walking/Spirit_walking_01_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,9,12</rect>
                <key>scale9Paddings</key>
                <rect>5,6,9,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,9,12</rect>
                <key>scale9Paddings</key>
                <rect>5,6,9,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,9,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,9,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,9,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,9,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,12,9,24</rect>
                <key>scale9Paddings</key>
                <rect>5,12,9,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,12,9,24</rect>
                <key>scale9Paddings</key>
                <rect>5,12,9,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,8,3</rect>
                <key>scale9Paddings</key>
                <rect>4,2,8,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Death/Spirit_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,8,3</rect>
                <key>scale9Paddings</key>
                <rect>4,2,8,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Hit/Spirit_hit_02.png</key>
            <key type="filename">Spirit/Hit/Spirit_hit_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Hit/Spirit_hit_02_n.png</key>
            <key type="filename">Spirit/Hit/Spirit_hit_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Idle/Spirit_idle_02.png</key>
            <key type="filename">Spirit/Walking/Spirit_walking_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,10</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Idle/Spirit_idle_02_n.png</key>
            <key type="filename">Spirit/Walking/Spirit_walking_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,10</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Idle/Spirit_idle_03.png</key>
            <key type="filename">Spirit/Walking/Spirit_walking_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,4,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,4,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Spirit/Idle/Spirit_idle_03_n.png</key>
            <key type="filename">Spirit/Walking/Spirit_walking_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,4,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,4,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_01.png</key>
            <key type="filename">Wolf/Death/Wolf_death_01.png</key>
            <key type="filename">Wolf/Hit/Wolf_hit_01.png</key>
            <key type="filename">Wolf/Idle/Wolf_idle_01.png</key>
            <key type="filename">Wolf/Idle/Wolf_idle_02.png</key>
            <key type="filename">Wolf/Idle/Wolf_idle_04.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,19,14</rect>
                <key>scale9Paddings</key>
                <rect>10,7,19,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_01_n.png</key>
            <key type="filename">Wolf/Death/Wolf_death_01_n.png</key>
            <key type="filename">Wolf/Hit/Wolf_hit_01_n.png</key>
            <key type="filename">Wolf/Idle/Wolf_idle_01_n.png</key>
            <key type="filename">Wolf/Idle/Wolf_idle_02_n.png</key>
            <key type="filename">Wolf/Idle/Wolf_idle_04_n.png</key>
            <key type="filename">Wolf/Walking/Wolf_walking_01_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,19,14</rect>
                <key>scale9Paddings</key>
                <rect>10,7,19,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,21,14</rect>
                <key>scale9Paddings</key>
                <rect>10,7,21,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,21,14</rect>
                <key>scale9Paddings</key>
                <rect>10,7,21,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,6,23,13</rect>
                <key>scale9Paddings</key>
                <rect>12,6,23,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,6,23,13</rect>
                <key>scale9Paddings</key>
                <rect>12,6,23,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,6,22,13</rect>
                <key>scale9Paddings</key>
                <rect>11,6,22,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Attack/Wolf_attack_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,6,22,13</rect>
                <key>scale9Paddings</key>
                <rect>11,6,22,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,6,20,12</rect>
                <key>scale9Paddings</key>
                <rect>10,6,20,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_02_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,6,20,12</rect>
                <key>scale9Paddings</key>
                <rect>10,6,20,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,5,19,10</rect>
                <key>scale9Paddings</key>
                <rect>10,5,19,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,5,19,10</rect>
                <key>scale9Paddings</key>
                <rect>10,5,19,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,4,17,9</rect>
                <key>scale9Paddings</key>
                <rect>9,4,17,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_04_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,4,17,9</rect>
                <key>scale9Paddings</key>
                <rect>9,4,17,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_05.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,13,6</rect>
                <key>scale9Paddings</key>
                <rect>7,3,13,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_05_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,13,6</rect>
                <key>scale9Paddings</key>
                <rect>7,3,13,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,8,4</rect>
                <key>scale9Paddings</key>
                <rect>4,2,8,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Death/Wolf_death_07_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,8,4</rect>
                <key>scale9Paddings</key>
                <rect>4,2,8,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Hit/Wolf_hit_02.png</key>
            <key type="filename">Wolf/Hit/Wolf_hit_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,7,18,14</rect>
                <key>scale9Paddings</key>
                <rect>9,7,18,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Hit/Wolf_hit_02_n.png</key>
            <key type="filename">Wolf/Hit/Wolf_hit_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,7,18,14</rect>
                <key>scale9Paddings</key>
                <rect>9,7,18,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Idle/Wolf_idle_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,1</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,19,13</rect>
                <key>scale9Paddings</key>
                <rect>10,7,19,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Wolf/Idle/Wolf_idle_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,7,19,13</rect>
                <key>scale9Paddings</key>
                <rect>10,7,19,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">bluelight HD.png</key>
            <key type="filename">bluelight HD_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,48,48</rect>
                <key>scale9Paddings</key>
                <rect>24,24,48,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <true/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
