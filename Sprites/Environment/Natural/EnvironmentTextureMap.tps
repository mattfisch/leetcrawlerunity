<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/vortech/workspace/git/leetcrawlerunity/Sprites/Environment/Natural/EnvironmentTextureMap.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>1</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>1</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Polygon</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../LeetCrawler/Assets/Sprites/Environment/Dungeon.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>2</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">CropKeepPos</enum>
            <key>tracerTolerance</key>
            <int>50</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">1.png</key>
            <key type="filename">100.png</key>
            <key type="filename">100_n.png</key>
            <key type="filename">102.png</key>
            <key type="filename">102_n.png</key>
            <key type="filename">103.png</key>
            <key type="filename">103_n.png</key>
            <key type="filename">104.png</key>
            <key type="filename">104_n.png</key>
            <key type="filename">105.png</key>
            <key type="filename">105_n.png</key>
            <key type="filename">107.png</key>
            <key type="filename">107_n.png</key>
            <key type="filename">109.png</key>
            <key type="filename">109_n.png</key>
            <key type="filename">110.png</key>
            <key type="filename">110_n.png</key>
            <key type="filename">111.png</key>
            <key type="filename">111_n.png</key>
            <key type="filename">113.png</key>
            <key type="filename">113_n.png</key>
            <key type="filename">115.png</key>
            <key type="filename">115_n.png</key>
            <key type="filename">116.png</key>
            <key type="filename">116_n.png</key>
            <key type="filename">118.png</key>
            <key type="filename">118_n.png</key>
            <key type="filename">119.png</key>
            <key type="filename">119_n.png</key>
            <key type="filename">120.png</key>
            <key type="filename">120_n.png</key>
            <key type="filename">121.png</key>
            <key type="filename">121_n.png</key>
            <key type="filename">123.png</key>
            <key type="filename">123_n.png</key>
            <key type="filename">125.png</key>
            <key type="filename">125_n.png</key>
            <key type="filename">126.png</key>
            <key type="filename">126_n.png</key>
            <key type="filename">129.png</key>
            <key type="filename">129_n.png</key>
            <key type="filename">130.png</key>
            <key type="filename">130_n.png</key>
            <key type="filename">131.png</key>
            <key type="filename">131_n.png</key>
            <key type="filename">132.png</key>
            <key type="filename">132_n.png</key>
            <key type="filename">134.png</key>
            <key type="filename">134_n.png</key>
            <key type="filename">135.png</key>
            <key type="filename">135_n.png</key>
            <key type="filename">139.png</key>
            <key type="filename">139_n.png</key>
            <key type="filename">141.png</key>
            <key type="filename">141_n.png</key>
            <key type="filename">142.png</key>
            <key type="filename">142_n.png</key>
            <key type="filename">143.png</key>
            <key type="filename">143_n.png</key>
            <key type="filename">15.png</key>
            <key type="filename">150.png</key>
            <key type="filename">150_n.png</key>
            <key type="filename">151.png</key>
            <key type="filename">151_n.png</key>
            <key type="filename">152.png</key>
            <key type="filename">152_n.png</key>
            <key type="filename">157.png</key>
            <key type="filename">157_n.png</key>
            <key type="filename">158.png</key>
            <key type="filename">158_n.png</key>
            <key type="filename">159.png</key>
            <key type="filename">159_n.png</key>
            <key type="filename">15_n.png</key>
            <key type="filename">16.png</key>
            <key type="filename">166.png</key>
            <key type="filename">166_n.png</key>
            <key type="filename">168.png</key>
            <key type="filename">168_n.png</key>
            <key type="filename">169.png</key>
            <key type="filename">169_n.png</key>
            <key type="filename">16_n.png</key>
            <key type="filename">17.png</key>
            <key type="filename">17_n.png</key>
            <key type="filename">18.png</key>
            <key type="filename">18_n.png</key>
            <key type="filename">19.png</key>
            <key type="filename">19_n.png</key>
            <key type="filename">1_n.png</key>
            <key type="filename">2.png</key>
            <key type="filename">20.png</key>
            <key type="filename">209.png</key>
            <key type="filename">209_n.png</key>
            <key type="filename">20_n.png</key>
            <key type="filename">21.png</key>
            <key type="filename">21_n.png</key>
            <key type="filename">22.png</key>
            <key type="filename">22_n.png</key>
            <key type="filename">23.png</key>
            <key type="filename">23_n.png</key>
            <key type="filename">24.png</key>
            <key type="filename">24_n.png</key>
            <key type="filename">26.png</key>
            <key type="filename">26_n.png</key>
            <key type="filename">29.png</key>
            <key type="filename">29_n.png</key>
            <key type="filename">2_n.png</key>
            <key type="filename">3.png</key>
            <key type="filename">30.png</key>
            <key type="filename">30_n.png</key>
            <key type="filename">31.png</key>
            <key type="filename">31_n.png</key>
            <key type="filename">32.png</key>
            <key type="filename">32_n.png</key>
            <key type="filename">33.png</key>
            <key type="filename">33_n.png</key>
            <key type="filename">36.png</key>
            <key type="filename">36_n.png</key>
            <key type="filename">37.png</key>
            <key type="filename">37_n.png</key>
            <key type="filename">3_n.png</key>
            <key type="filename">4.png</key>
            <key type="filename">42.png</key>
            <key type="filename">42_n.png</key>
            <key type="filename">48.png</key>
            <key type="filename">48_n.png</key>
            <key type="filename">49.png</key>
            <key type="filename">49_n.png</key>
            <key type="filename">4_n.png</key>
            <key type="filename">50.png</key>
            <key type="filename">50_n.png</key>
            <key type="filename">51.png</key>
            <key type="filename">51_n.png</key>
            <key type="filename">52.png</key>
            <key type="filename">52_n.png</key>
            <key type="filename">53.png</key>
            <key type="filename">53_n.png</key>
            <key type="filename">54.png</key>
            <key type="filename">54_n.png</key>
            <key type="filename">55.png</key>
            <key type="filename">55_n.png</key>
            <key type="filename">56.png</key>
            <key type="filename">56_n.png</key>
            <key type="filename">57.png</key>
            <key type="filename">57_n.png</key>
            <key type="filename">6.png</key>
            <key type="filename">60.png</key>
            <key type="filename">60_n.png</key>
            <key type="filename">61.png</key>
            <key type="filename">61_n.png</key>
            <key type="filename">62.png</key>
            <key type="filename">62_n.png</key>
            <key type="filename">63.png</key>
            <key type="filename">63_n.png</key>
            <key type="filename">65.png</key>
            <key type="filename">65_n.png</key>
            <key type="filename">66.png</key>
            <key type="filename">66_n.png</key>
            <key type="filename">67.png</key>
            <key type="filename">67_n.png</key>
            <key type="filename">68.png</key>
            <key type="filename">68_n.png</key>
            <key type="filename">69.png</key>
            <key type="filename">69_n.png</key>
            <key type="filename">6_n.png</key>
            <key type="filename">7.png</key>
            <key type="filename">70.png</key>
            <key type="filename">70_n.png</key>
            <key type="filename">73.png</key>
            <key type="filename">73_n.png</key>
            <key type="filename">74.png</key>
            <key type="filename">74_n.png</key>
            <key type="filename">76.png</key>
            <key type="filename">76_n.png</key>
            <key type="filename">79.png</key>
            <key type="filename">79_n.png</key>
            <key type="filename">7_n.png</key>
            <key type="filename">8.png</key>
            <key type="filename">81.png</key>
            <key type="filename">81_n.png</key>
            <key type="filename">82.png</key>
            <key type="filename">82_n.png</key>
            <key type="filename">83.png</key>
            <key type="filename">83_n.png</key>
            <key type="filename">84.png</key>
            <key type="filename">84_n.png</key>
            <key type="filename">86.png</key>
            <key type="filename">86_n.png</key>
            <key type="filename">88.png</key>
            <key type="filename">88_n.png</key>
            <key type="filename">89.png</key>
            <key type="filename">89_n.png</key>
            <key type="filename">8_n.png</key>
            <key type="filename">91.png</key>
            <key type="filename">91_n.png</key>
            <key type="filename">93.png</key>
            <key type="filename">93_n.png</key>
            <key type="filename">97.png</key>
            <key type="filename">97_n.png</key>
            <key type="filename">98.png</key>
            <key type="filename">98_n.png</key>
            <key type="filename">99.png</key>
            <key type="filename">99_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Flower_01_01.png</key>
            <key type="filename">Flower_01_01_n.png</key>
            <key type="filename">Flower_01_02.png</key>
            <key type="filename">Flower_01_02_n.png</key>
            <key type="filename">Flower_01_03.png</key>
            <key type="filename">Flower_01_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,16,26,31</rect>
                <key>scale9Paddings</key>
                <rect>13,16,26,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Flower_02_01.png</key>
            <key type="filename">Flower_02_01_n.png</key>
            <key type="filename">Flower_02_02.png</key>
            <key type="filename">Flower_02_02_n.png</key>
            <key type="filename">Flower_02_03.png</key>
            <key type="filename">Flower_02_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,11,24,22</rect>
                <key>scale9Paddings</key>
                <rect>12,11,24,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Mushrooms_01_01.png</key>
            <key type="filename">Mushrooms_01_01_n.png</key>
            <key type="filename">Mushrooms_01_02.png</key>
            <key type="filename">Mushrooms_01_02_n.png</key>
            <key type="filename">Mushrooms_01_03.png</key>
            <key type="filename">Mushrooms_01_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,9,29,18</rect>
                <key>scale9Paddings</key>
                <rect>14,9,29,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Mushrooms_02_01.png</key>
            <key type="filename">Mushrooms_02_01_n.png</key>
            <key type="filename">Mushrooms_02_02.png</key>
            <key type="filename">Mushrooms_02_02_n.png</key>
            <key type="filename">Mushrooms_02_03.png</key>
            <key type="filename">Mushrooms_02_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,12,29,23</rect>
                <key>scale9Paddings</key>
                <rect>15,12,29,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Tree_01_01.png</key>
            <key type="filename">Tree_01_01_n.png</key>
            <key type="filename">Tree_01_02.png</key>
            <key type="filename">Tree_01_02_n.png</key>
            <key type="filename">Tree_01_03.png</key>
            <key type="filename">Tree_01_03_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,63,96,125</rect>
                <key>scale9Paddings</key>
                <rect>48,63,96,125</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>1_n.png</filename>
            <filename>1.png</filename>
            <filename>2_n.png</filename>
            <filename>2.png</filename>
            <filename>3_n.png</filename>
            <filename>3.png</filename>
            <filename>4_n.png</filename>
            <filename>4.png</filename>
            <filename>6_n.png</filename>
            <filename>6.png</filename>
            <filename>7_n.png</filename>
            <filename>7.png</filename>
            <filename>8_n.png</filename>
            <filename>8.png</filename>
            <filename>15_n.png</filename>
            <filename>15.png</filename>
            <filename>16_n.png</filename>
            <filename>16.png</filename>
            <filename>17_n.png</filename>
            <filename>17.png</filename>
            <filename>18_n.png</filename>
            <filename>18.png</filename>
            <filename>19_n.png</filename>
            <filename>19.png</filename>
            <filename>20_n.png</filename>
            <filename>20.png</filename>
            <filename>21_n.png</filename>
            <filename>21.png</filename>
            <filename>22_n.png</filename>
            <filename>22.png</filename>
            <filename>23_n.png</filename>
            <filename>23.png</filename>
            <filename>24_n.png</filename>
            <filename>24.png</filename>
            <filename>26_n.png</filename>
            <filename>26.png</filename>
            <filename>29_n.png</filename>
            <filename>29.png</filename>
            <filename>30_n.png</filename>
            <filename>30.png</filename>
            <filename>31_n.png</filename>
            <filename>31.png</filename>
            <filename>32_n.png</filename>
            <filename>32.png</filename>
            <filename>33_n.png</filename>
            <filename>33.png</filename>
            <filename>36.png</filename>
            <filename>37_n.png</filename>
            <filename>37.png</filename>
            <filename>42_n.png</filename>
            <filename>42.png</filename>
            <filename>48_n.png</filename>
            <filename>48.png</filename>
            <filename>49_n.png</filename>
            <filename>49.png</filename>
            <filename>50_n.png</filename>
            <filename>50.png</filename>
            <filename>51_n.png</filename>
            <filename>51.png</filename>
            <filename>52_n.png</filename>
            <filename>52.png</filename>
            <filename>53_n.png</filename>
            <filename>53.png</filename>
            <filename>54_n.png</filename>
            <filename>54.png</filename>
            <filename>55_n.png</filename>
            <filename>55.png</filename>
            <filename>56_n.png</filename>
            <filename>56.png</filename>
            <filename>57_n.png</filename>
            <filename>57.png</filename>
            <filename>60_n.png</filename>
            <filename>60.png</filename>
            <filename>61_n.png</filename>
            <filename>61.png</filename>
            <filename>62_n.png</filename>
            <filename>62.png</filename>
            <filename>63_n.png</filename>
            <filename>63.png</filename>
            <filename>65_n.png</filename>
            <filename>65.png</filename>
            <filename>66_n.png</filename>
            <filename>66.png</filename>
            <filename>67_n.png</filename>
            <filename>67.png</filename>
            <filename>68_n.png</filename>
            <filename>68.png</filename>
            <filename>69_n.png</filename>
            <filename>69.png</filename>
            <filename>70_n.png</filename>
            <filename>70.png</filename>
            <filename>73_n.png</filename>
            <filename>73.png</filename>
            <filename>74_n.png</filename>
            <filename>74.png</filename>
            <filename>76_n.png</filename>
            <filename>76.png</filename>
            <filename>79_n.png</filename>
            <filename>79.png</filename>
            <filename>81_n.png</filename>
            <filename>81.png</filename>
            <filename>82_n.png</filename>
            <filename>82.png</filename>
            <filename>83_n.png</filename>
            <filename>83.png</filename>
            <filename>84_n.png</filename>
            <filename>84.png</filename>
            <filename>86_n.png</filename>
            <filename>86.png</filename>
            <filename>88_n.png</filename>
            <filename>88.png</filename>
            <filename>89_n.png</filename>
            <filename>89.png</filename>
            <filename>91_n.png</filename>
            <filename>91.png</filename>
            <filename>93_n.png</filename>
            <filename>93.png</filename>
            <filename>97_n.png</filename>
            <filename>97.png</filename>
            <filename>98_n.png</filename>
            <filename>98.png</filename>
            <filename>99_n.png</filename>
            <filename>99.png</filename>
            <filename>100_n.png</filename>
            <filename>100.png</filename>
            <filename>102_n.png</filename>
            <filename>102.png</filename>
            <filename>103_n.png</filename>
            <filename>103.png</filename>
            <filename>104_n.png</filename>
            <filename>104.png</filename>
            <filename>105_n.png</filename>
            <filename>105.png</filename>
            <filename>107_n.png</filename>
            <filename>107.png</filename>
            <filename>109_n.png</filename>
            <filename>109.png</filename>
            <filename>110_n.png</filename>
            <filename>110.png</filename>
            <filename>111_n.png</filename>
            <filename>111.png</filename>
            <filename>113_n.png</filename>
            <filename>113.png</filename>
            <filename>115_n.png</filename>
            <filename>115.png</filename>
            <filename>116_n.png</filename>
            <filename>116.png</filename>
            <filename>118_n.png</filename>
            <filename>118.png</filename>
            <filename>119_n.png</filename>
            <filename>119.png</filename>
            <filename>120_n.png</filename>
            <filename>120.png</filename>
            <filename>121_n.png</filename>
            <filename>121.png</filename>
            <filename>123_n.png</filename>
            <filename>123.png</filename>
            <filename>125_n.png</filename>
            <filename>125.png</filename>
            <filename>126_n.png</filename>
            <filename>126.png</filename>
            <filename>129_n.png</filename>
            <filename>129.png</filename>
            <filename>130_n.png</filename>
            <filename>130.png</filename>
            <filename>131_n.png</filename>
            <filename>131.png</filename>
            <filename>132_n.png</filename>
            <filename>132.png</filename>
            <filename>134_n.png</filename>
            <filename>134.png</filename>
            <filename>135_n.png</filename>
            <filename>135.png</filename>
            <filename>139_n.png</filename>
            <filename>139.png</filename>
            <filename>141_n.png</filename>
            <filename>141.png</filename>
            <filename>142_n.png</filename>
            <filename>142.png</filename>
            <filename>143_n.png</filename>
            <filename>143.png</filename>
            <filename>150_n.png</filename>
            <filename>150.png</filename>
            <filename>151_n.png</filename>
            <filename>151.png</filename>
            <filename>152_n.png</filename>
            <filename>152.png</filename>
            <filename>157_n.png</filename>
            <filename>157.png</filename>
            <filename>158_n.png</filename>
            <filename>158.png</filename>
            <filename>159_n.png</filename>
            <filename>159.png</filename>
            <filename>166_n.png</filename>
            <filename>166.png</filename>
            <filename>168_n.png</filename>
            <filename>168.png</filename>
            <filename>169_n.png</filename>
            <filename>169.png</filename>
            <filename>209_n.png</filename>
            <filename>209.png</filename>
            <filename>36_n.png</filename>
            <filename>Flower_01_01_n.png</filename>
            <filename>Flower_01_01.png</filename>
            <filename>Flower_01_02_n.png</filename>
            <filename>Flower_01_02.png</filename>
            <filename>Flower_01_03_n.png</filename>
            <filename>Flower_01_03.png</filename>
            <filename>Flower_02_01_n.png</filename>
            <filename>Flower_02_01.png</filename>
            <filename>Flower_02_02_n.png</filename>
            <filename>Flower_02_02.png</filename>
            <filename>Flower_02_03_n.png</filename>
            <filename>Flower_02_03.png</filename>
            <filename>Mushrooms_01_01_n.png</filename>
            <filename>Mushrooms_01_01.png</filename>
            <filename>Mushrooms_01_02_n.png</filename>
            <filename>Mushrooms_01_02.png</filename>
            <filename>Mushrooms_01_03_n.png</filename>
            <filename>Mushrooms_01_03.png</filename>
            <filename>Mushrooms_02_01_n.png</filename>
            <filename>Mushrooms_02_01.png</filename>
            <filename>Mushrooms_02_02_n.png</filename>
            <filename>Mushrooms_02_02.png</filename>
            <filename>Mushrooms_02_03_n.png</filename>
            <filename>Mushrooms_02_03.png</filename>
            <filename>Tree_01_01_n.png</filename>
            <filename>Tree_01_01.png</filename>
            <filename>Tree_01_02_n.png</filename>
            <filename>Tree_01_02.png</filename>
            <filename>Tree_01_03_n.png</filename>
            <filename>Tree_01_03.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <true/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
