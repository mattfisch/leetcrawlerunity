<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/vortech/workspace/git/leetcrawlerunity/Sprites/Environment/Loot/LootTextureMap.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../LeetCrawler/Assets/Sprites/Environment/Loot.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">0.png</key>
            <key type="filename">0_n.png</key>
            <key type="filename">1.png</key>
            <key type="filename">10.png</key>
            <key type="filename">10_n.png</key>
            <key type="filename">11.png</key>
            <key type="filename">11_n.png</key>
            <key type="filename">12.png</key>
            <key type="filename">12_n.png</key>
            <key type="filename">13.png</key>
            <key type="filename">13_n.png</key>
            <key type="filename">14.png</key>
            <key type="filename">14_n.png</key>
            <key type="filename">15.png</key>
            <key type="filename">15_n.png</key>
            <key type="filename">16.png</key>
            <key type="filename">16_n.png</key>
            <key type="filename">17.png</key>
            <key type="filename">17_n.png</key>
            <key type="filename">18.png</key>
            <key type="filename">18_n.png</key>
            <key type="filename">19.png</key>
            <key type="filename">19_n.png</key>
            <key type="filename">1_n.png</key>
            <key type="filename">2.png</key>
            <key type="filename">20.png</key>
            <key type="filename">20_n.png</key>
            <key type="filename">21.png</key>
            <key type="filename">21_n.png</key>
            <key type="filename">22.png</key>
            <key type="filename">22_n.png</key>
            <key type="filename">23.png</key>
            <key type="filename">23_n.png</key>
            <key type="filename">24.png</key>
            <key type="filename">24_n.png</key>
            <key type="filename">25.png</key>
            <key type="filename">25_n.png</key>
            <key type="filename">26.png</key>
            <key type="filename">26_n.png</key>
            <key type="filename">27.png</key>
            <key type="filename">27_n.png</key>
            <key type="filename">28.png</key>
            <key type="filename">28_n.png</key>
            <key type="filename">29.png</key>
            <key type="filename">29_n.png</key>
            <key type="filename">2_n.png</key>
            <key type="filename">3.png</key>
            <key type="filename">30.png</key>
            <key type="filename">30_n.png</key>
            <key type="filename">31.png</key>
            <key type="filename">31_n.png</key>
            <key type="filename">32.png</key>
            <key type="filename">32_n.png</key>
            <key type="filename">33.png</key>
            <key type="filename">33_n.png</key>
            <key type="filename">34.png</key>
            <key type="filename">34_n.png</key>
            <key type="filename">35.png</key>
            <key type="filename">35_n.png</key>
            <key type="filename">36.png</key>
            <key type="filename">36_n.png</key>
            <key type="filename">37.png</key>
            <key type="filename">37_n.png</key>
            <key type="filename">38.png</key>
            <key type="filename">38_n.png</key>
            <key type="filename">39.png</key>
            <key type="filename">39_n.png</key>
            <key type="filename">3_n.png</key>
            <key type="filename">4.png</key>
            <key type="filename">40.png</key>
            <key type="filename">40_n.png</key>
            <key type="filename">41.png</key>
            <key type="filename">41_n.png</key>
            <key type="filename">42.png</key>
            <key type="filename">42_n.png</key>
            <key type="filename">43.png</key>
            <key type="filename">43_n.png</key>
            <key type="filename">44.png</key>
            <key type="filename">44_n.png</key>
            <key type="filename">45.png</key>
            <key type="filename">45_n.png</key>
            <key type="filename">46.png</key>
            <key type="filename">46_n.png</key>
            <key type="filename">47.png</key>
            <key type="filename">47_n.png</key>
            <key type="filename">48.png</key>
            <key type="filename">48_n.png</key>
            <key type="filename">49.png</key>
            <key type="filename">49_n.png</key>
            <key type="filename">4_n.png</key>
            <key type="filename">5.png</key>
            <key type="filename">50.png</key>
            <key type="filename">50_n.png</key>
            <key type="filename">51.png</key>
            <key type="filename">51_n.png</key>
            <key type="filename">52.png</key>
            <key type="filename">52_n.png</key>
            <key type="filename">53.png</key>
            <key type="filename">53_n.png</key>
            <key type="filename">54.png</key>
            <key type="filename">54_n.png</key>
            <key type="filename">55.png</key>
            <key type="filename">55_n.png</key>
            <key type="filename">56.png</key>
            <key type="filename">56_n.png</key>
            <key type="filename">57.png</key>
            <key type="filename">57_n.png</key>
            <key type="filename">58.png</key>
            <key type="filename">58_n.png</key>
            <key type="filename">59.png</key>
            <key type="filename">59_n.png</key>
            <key type="filename">5_n.png</key>
            <key type="filename">6.png</key>
            <key type="filename">60.png</key>
            <key type="filename">60_n.png</key>
            <key type="filename">61.png</key>
            <key type="filename">61_n.png</key>
            <key type="filename">62.png</key>
            <key type="filename">62_n.png</key>
            <key type="filename">63.png</key>
            <key type="filename">63_n.png</key>
            <key type="filename">64.png</key>
            <key type="filename">64_n.png</key>
            <key type="filename">65.png</key>
            <key type="filename">65_n.png</key>
            <key type="filename">66.png</key>
            <key type="filename">66_n.png</key>
            <key type="filename">67.png</key>
            <key type="filename">67_n.png</key>
            <key type="filename">68.png</key>
            <key type="filename">68_n.png</key>
            <key type="filename">69.png</key>
            <key type="filename">69_n.png</key>
            <key type="filename">6_n.png</key>
            <key type="filename">7.png</key>
            <key type="filename">71.png</key>
            <key type="filename">71_n.png</key>
            <key type="filename">7_n.png</key>
            <key type="filename">8.png</key>
            <key type="filename">8_n.png</key>
            <key type="filename">9.png</key>
            <key type="filename">9_n.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>0_n.png</filename>
            <filename>0.png</filename>
            <filename>1_n.png</filename>
            <filename>1.png</filename>
            <filename>2_n.png</filename>
            <filename>2.png</filename>
            <filename>3_n.png</filename>
            <filename>3.png</filename>
            <filename>4_n.png</filename>
            <filename>4.png</filename>
            <filename>5_n.png</filename>
            <filename>5.png</filename>
            <filename>6_n.png</filename>
            <filename>6.png</filename>
            <filename>7_n.png</filename>
            <filename>7.png</filename>
            <filename>8_n.png</filename>
            <filename>8.png</filename>
            <filename>9_n.png</filename>
            <filename>9.png</filename>
            <filename>10_n.png</filename>
            <filename>10.png</filename>
            <filename>11_n.png</filename>
            <filename>11.png</filename>
            <filename>12_n.png</filename>
            <filename>12.png</filename>
            <filename>13_n.png</filename>
            <filename>13.png</filename>
            <filename>14_n.png</filename>
            <filename>14.png</filename>
            <filename>15_n.png</filename>
            <filename>15.png</filename>
            <filename>16_n.png</filename>
            <filename>16.png</filename>
            <filename>17_n.png</filename>
            <filename>17.png</filename>
            <filename>18_n.png</filename>
            <filename>18.png</filename>
            <filename>19_n.png</filename>
            <filename>19.png</filename>
            <filename>20_n.png</filename>
            <filename>20.png</filename>
            <filename>21_n.png</filename>
            <filename>21.png</filename>
            <filename>22_n.png</filename>
            <filename>22.png</filename>
            <filename>23_n.png</filename>
            <filename>23.png</filename>
            <filename>24_n.png</filename>
            <filename>24.png</filename>
            <filename>25_n.png</filename>
            <filename>25.png</filename>
            <filename>26_n.png</filename>
            <filename>26.png</filename>
            <filename>27_n.png</filename>
            <filename>27.png</filename>
            <filename>28_n.png</filename>
            <filename>28.png</filename>
            <filename>29_n.png</filename>
            <filename>29.png</filename>
            <filename>30_n.png</filename>
            <filename>30.png</filename>
            <filename>31_n.png</filename>
            <filename>31.png</filename>
            <filename>32_n.png</filename>
            <filename>32.png</filename>
            <filename>33_n.png</filename>
            <filename>33.png</filename>
            <filename>34_n.png</filename>
            <filename>34.png</filename>
            <filename>35_n.png</filename>
            <filename>35.png</filename>
            <filename>36_n.png</filename>
            <filename>36.png</filename>
            <filename>37_n.png</filename>
            <filename>37.png</filename>
            <filename>38_n.png</filename>
            <filename>38.png</filename>
            <filename>39_n.png</filename>
            <filename>39.png</filename>
            <filename>40_n.png</filename>
            <filename>40.png</filename>
            <filename>41_n.png</filename>
            <filename>41.png</filename>
            <filename>42_n.png</filename>
            <filename>42.png</filename>
            <filename>43_n.png</filename>
            <filename>43.png</filename>
            <filename>44_n.png</filename>
            <filename>44.png</filename>
            <filename>45_n.png</filename>
            <filename>45.png</filename>
            <filename>46_n.png</filename>
            <filename>46.png</filename>
            <filename>47_n.png</filename>
            <filename>47.png</filename>
            <filename>48_n.png</filename>
            <filename>48.png</filename>
            <filename>49_n.png</filename>
            <filename>49.png</filename>
            <filename>50_n.png</filename>
            <filename>50.png</filename>
            <filename>51_n.png</filename>
            <filename>51.png</filename>
            <filename>52_n.png</filename>
            <filename>52.png</filename>
            <filename>53_n.png</filename>
            <filename>53.png</filename>
            <filename>54_n.png</filename>
            <filename>54.png</filename>
            <filename>55_n.png</filename>
            <filename>55.png</filename>
            <filename>56_n.png</filename>
            <filename>56.png</filename>
            <filename>57_n.png</filename>
            <filename>57.png</filename>
            <filename>58_n.png</filename>
            <filename>58.png</filename>
            <filename>59_n.png</filename>
            <filename>59.png</filename>
            <filename>60_n.png</filename>
            <filename>60.png</filename>
            <filename>61_n.png</filename>
            <filename>61.png</filename>
            <filename>62_n.png</filename>
            <filename>62.png</filename>
            <filename>63_n.png</filename>
            <filename>63.png</filename>
            <filename>64_n.png</filename>
            <filename>64.png</filename>
            <filename>65_n.png</filename>
            <filename>65.png</filename>
            <filename>66_n.png</filename>
            <filename>66.png</filename>
            <filename>67_n.png</filename>
            <filename>67.png</filename>
            <filename>68_n.png</filename>
            <filename>68.png</filename>
            <filename>69_n.png</filename>
            <filename>69.png</filename>
            <filename>71_n.png</filename>
            <filename>71.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <true/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
