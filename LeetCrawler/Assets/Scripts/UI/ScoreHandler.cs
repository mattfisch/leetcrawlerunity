﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHandler : MonoBehaviour
{
    private string playerName;
    private Text score;
    private InputField inputField;
    private List<Score> highScores;
    private bool firstEdit = true;

    void Awake()
    {
        TouchScreenKeyboard.hideInput = true;
        inputField = GameObject.Find("InputPlayerName").GetComponent<InputField>();
    }

    void Start()
    {
        score = GetComponent<Text>();
        score.text = ScoreController.scoreval.ToString();
        TouchScreenKeyboard.hideInput = true;
        
        // Init highscore list on first app start
        if (!PlayerPrefs.HasKey("HighscoreFirstInint"))
        {
            HighscoreController.Instance.ClearHighScoreList();
            PlayerPrefs.SetInt("HighscoreFirstInint", 1);
            PlayerPrefs.Save();
        }

        highScores = HighscoreController.Instance.GetHighScoreList();

        if (ScoreController.scoreval < highScores[highScores.Count -1].playerScore)
        {
            GameObject.Find("InputPlayerName").SetActive(false);
            GameObject.Find("PlayerName").SetActive(false);
        }
        else
        {
            GameObject.Find("ScoreCaption").GetComponent<Text>().text = "New Highscore:";
            GameObject.Find("InputPlayerName").SetActive(true);
            GameObject.Find("PlayerName").SetActive(true);
        }

        inputField.onValueChanged.AddListener(TextChanged);
        inputField.onEndEdit.AddListener(delegate { SaveList(); });
    }

    public void TextChanged(string newName)
    {
        TouchScreenKeyboard.hideInput = true;
        playerName = newName;
    }

    public void SaveList()
    {
        if (firstEdit)
        {
            firstEdit = false;

            GameObject.Find("InputPlayerName").SetActive(false);
            GameObject.Find("PlayerName").SetActive(false);

            if(playerName.Length <= 0)
            {
                playerName = "Player1";
            }
            Score temp = new Score(playerName, ScoreController.scoreval);
            highScores.Add(temp);
            highScores.Sort((a, b) => b.playerScore.CompareTo(a.playerScore));

            HighscoreController.Instance.WriteHighScoreList(highScores);
        }
    }
}
