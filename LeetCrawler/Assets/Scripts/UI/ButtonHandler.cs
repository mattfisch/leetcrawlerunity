﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonHandler : MonoBehaviour
{
    public static string Level;

    public void OnArenaModeClicked()
    {
        Level = "Arena";
        SceneManager.LoadScene("Arena");
    }

    public void OnStoryModeClicked()
    {
        Level = "Story";
        SceneManager.LoadScene("Story");
    }

    public void OnQuitGameClicked()
    {
        Application.Quit();
    }

    public void OnRestartGameClicked()
    {
        SceneManager.LoadScene(Level);
    }

    public void OnBackToMainMenuClicked()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnHighscoreClicked()
    {
        SceneManager.LoadScene("Highscores");
    }
}
