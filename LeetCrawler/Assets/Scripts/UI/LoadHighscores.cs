﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadHighscores : MonoBehaviour
{
    public Text[] playerNames;
    public Text[] playerScores;

    private List<Score> highScores;

    void Start () {

        highScores = HighscoreController.Instance.GetHighScoreList();
       
        for (int i = 0; i < highScores.Count; i++)
        {
            playerNames[i].text =
                highScores[i].playerName;
            playerScores[i].text =
                highScores[i].playerScore.ToString();

        }
    }
}
