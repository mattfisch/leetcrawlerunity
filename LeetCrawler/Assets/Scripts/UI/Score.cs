﻿public class Score {

    public string playerName { get; set; }
    public int playerScore { get; set; }

    public Score (string name, int score)
    {
        playerName = name;
        playerScore = score;
    }

    public string toString()
    {
        return "Player: " + this.playerName + " / Score: " + this.playerScore;
    }
}
