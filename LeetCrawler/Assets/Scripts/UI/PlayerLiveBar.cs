﻿using UnityEngine;

public class PlayerLiveBar
{
    private readonly int maxHealth = 5;
    private int CurrentHealth;

    public PlayerLiveBar(int lives)
    {
        CurrentHealth = lives;
    }

    public int GetCurrentHealth()
    {
        return CurrentHealth;
    }

    public void ChangeHealth(int changeHealth)
    {
        CurrentHealth += changeHealth;
        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
        }
        else if (CurrentHealth >= maxHealth)
        {
            CurrentHealth = maxHealth;
        }
    }
}
