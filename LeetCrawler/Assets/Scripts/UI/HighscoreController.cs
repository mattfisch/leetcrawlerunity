﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// High score controller.
/// </summary>
public class HighscoreController : MonoBehaviour {

    private static HighscoreController _highscoreInstance;
    private const int MaxHighscores = 5;

    public static HighscoreController Instance
    {
        get
        {
            if (_highscoreInstance == null)
            {
                _highscoreInstance = new GameObject("HighscoreController").AddComponent<HighscoreController>();
            }
            return _highscoreInstance;
        }
    }

    void Awake()
    {
        if (_highscoreInstance == null)
        {
            _highscoreInstance = this;
        }
        else if (_highscoreInstance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void WriteHighScoreList(List<Score> highscores)
    {
        for (int i = 0; i < MaxHighscores; i++)
        {
            PlayerPrefs.SetString("HighScore" + i + "name", highscores[i].playerName);
            PlayerPrefs.SetInt("HighScore" + i + "score", highscores[i].playerScore);
        }
        PlayerPrefs.Save();
    }

    public List<Score> GetHighScoreList()
    {
        List<Score> highScores = new List<Score>();

        for(int i = 0; i < MaxHighscores; i++)
        {
            if(PlayerPrefs.HasKey("HighScore" + i + "score"))
            {
                Score temp = new Score(
                    PlayerPrefs.GetString("HighScore" + i + "name"),
                    PlayerPrefs.GetInt("HighScore" + i + "score") );
                
                highScores.Add(temp);
            }
        }

        return highScores;
    }

    public void ClearHighScoreList()
    {
        for (int i = 0; i < MaxHighscores; i++)
        {
            PlayerPrefs.SetString("HighScore" + i + "name", "Player1");
            PlayerPrefs.SetInt("HighScore" + i + "score", (5000 - (i * 1000)) );
        }
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.Save();
    }
}
