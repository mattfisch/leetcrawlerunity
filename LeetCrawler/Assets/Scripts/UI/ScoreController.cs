﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    private Text scoretext;
    public static int scoreval;
    private int Score
    {
        set
        {
            scoreval = value;
            scoretext.text = "Score: " + scoreval.ToString();
        }
        get
        {
            return scoreval;
        }
    }

    void Start()
    {
        scoretext = GameObject.Find("Canvas/ScoreText").GetComponent<Text>();
        Score = 0;
        EventManager.AddListener<ChangeScoreEvent>(TouchedLoot);
    }

    private void TouchedLoot(ChangeScoreEvent evt)
    {
        Score += evt.value;
    }
}
