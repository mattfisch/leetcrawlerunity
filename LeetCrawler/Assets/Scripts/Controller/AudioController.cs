﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class AudioController : MonoBehaviour
{

    public AudioClip[] Soundtrack;
    public AudioClip PlayerHasFinished;
    public AudioClip PlayerAttacks;
    public AudioClip PlayerHasDied;
    public AudioClip PlayerTakesDamage;
    public AudioClip PlayerBlocksHit;
    public AudioClip PlayerPickupLoot;
    public AudioClip EnemyAttacks;
    public AudioClip EnemyHasDied;
    public AudioClip EnemyTakesDamage;

    private AudioSource audio;

    void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    void Start()
    {
        EventManager.AddListener<PlayerFinishedEvent>(PlayerFinished);
        EventManager.AddListener<PlayerDiedEvent>(PlayerDiedEvent);
        EventManager.AddListener<PlayerHitEvent>(PlayerHit);
        EventManager.AddListener<PlayerAttackEvent>(PlayerAttack);
        EventManager.AddListener<PickupLootEvent>(PickupLoot);
        EventManager.AddListener<EnemyHitEvent>(EnemyHit);
        EventManager.AddListener<EnemyAttackEvent>(EnemyAttack);
        EventManager.AddListener<EnemyDiedEvent>(EnemyDied);

        if (!audio.playOnAwake)
        {
            audio.clip = Soundtrack[Random.Range(0, Soundtrack.Length)];
            audio.Play();
        }
    }

    void Update()
    {
        if (!audio.isPlaying)
        {
            audio.clip = Soundtrack[Random.Range(0, Soundtrack.Length)];
            audio.Play();
        }
    }

    private void PlayerFinished(PlayerFinishedEvent e)
    {
        audio.Stop();
        audio.clip = PlayerHasFinished;
        audio.Play();
    }

    private void PlayerDiedEvent(PlayerDiedEvent e)
    {
        audio.Stop();
        audio.clip = PlayerHasDied;
        audio.Play();
    }

    private void PlayerHit(PlayerHitEvent e)
    {
        if (e.HitEvent == "Hit")
        {
            audio.PlayOneShot(PlayerTakesDamage);
        }
        if (e.HitEvent == "BlockedHit")
        {
            audio.PlayOneShot(PlayerBlocksHit);
        }
    }

    private void PlayerAttack(PlayerAttackEvent e)
    {
        audio.PlayOneShot(PlayerAttacks);
    }

    private void PickupLoot(PickupLootEvent e)
    {
        audio.PlayOneShot(PlayerPickupLoot);
    }

    private void EnemyHit(EnemyHitEvent e)
    {
        audio.PlayOneShot(EnemyTakesDamage);
    }

    private void EnemyAttack(EnemyAttackEvent e)
    {
        audio.PlayOneShot(EnemyAttacks);
    }

    private void EnemyDied(EnemyDiedEvent e)
    {
        audio.PlayOneShot(EnemyHasDied);
    }
}
