﻿using UnityEngine;

public class GameObjectCDManager : MonoBehaviour
{
    public GameObject currentEnemy;
    public bool shouldSpawnEnemies;

    public GameObject gameOverUI;
    public GameObject finishedGameUI;

    void Start()
    {
        EventManager.AddListener<EnemyDiedEvent>(EnemyDied);
        if (shouldSpawnEnemies)
            InvokeRepeating("SpawnEnemies", 3, 3);

        EventManager.TriggerEvent(new SpawnLootEvent());
        EventManager.AddListener<PlayerDiedEvent>(PlayerDied);
        EventManager.AddListener<PlayerFinishedEvent>(PlayerFinished);
    }

    void SpawnEnemies()
    {
        if (!shouldSpawnEnemies)
        {
            CancelInvoke("SpawnEnemies");
        }
        else
        {
            EventManager.TriggerEvent(new SpawnEnemyEvent(currentEnemy, 1));
        }
    }

    void EnemyDied(EnemyDiedEvent e)
    {
        EnemyBaseAI ai = e.enemy.GetComponent<EnemyBaseAI>();

        EventManager.TriggerEvent(new ChangeScoreEvent(ai.worth));
        Destroy(e.enemy);
    }

    private void PlayerDied(PlayerDiedEvent e)
    {
        gameOverUI.SetActive(true);
        shouldSpawnEnemies = false;
    }

    private void PlayerFinished(PlayerFinishedEvent e)
    {
        finishedGameUI.SetActive(true);
        shouldSpawnEnemies = false;
    }
}
