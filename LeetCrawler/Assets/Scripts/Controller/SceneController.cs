﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public string Level;

    void Start()
    {
        EventManager.AddListener<SceneChangedEvent>(SceneChanged);
    }

    private void SceneChanged(SceneChangedEvent e)
    {
        Level = e.level;
        SceneManager.LoadScene(e.level);
    }
}
