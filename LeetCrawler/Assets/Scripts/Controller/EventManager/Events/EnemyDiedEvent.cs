﻿using UnityEngine;

public class EnemyDiedEvent : GameEvent
{
    public GameObject enemy;

    public EnemyDiedEvent(GameObject enemy)
    {
        this.enemy = enemy;
    }
}
