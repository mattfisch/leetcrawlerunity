﻿using System;
using UnityEngine;

public class SpawnEnemyEvent : GameEvent
{

    public GameObject enemy;
    public int amount;

    public SpawnEnemyEvent(GameObject enemy, int amount)
    {
        this.enemy = enemy;
        this.amount = amount;
    }
}

