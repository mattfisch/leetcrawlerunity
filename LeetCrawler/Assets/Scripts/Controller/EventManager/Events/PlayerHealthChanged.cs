﻿using System;

public class PlayerHealthChanged : GameEvent
{
    public int healthChanged;

    public PlayerHealthChanged(int healthChanged)
    {
        this.healthChanged = healthChanged;
    }
}

