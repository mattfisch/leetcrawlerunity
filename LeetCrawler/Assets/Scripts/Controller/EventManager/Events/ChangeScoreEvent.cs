﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScoreEvent : GameEvent
{
    public int value;

    public ChangeScoreEvent(int value)
    {
        this.value = value;
    }
}
