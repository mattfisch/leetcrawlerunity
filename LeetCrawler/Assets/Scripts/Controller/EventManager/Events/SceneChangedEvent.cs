﻿using System;

public class SceneChangedEvent : GameEvent
{
    public string level;

    public SceneChangedEvent(string level)
    {
        this.level = level;
    }
}

