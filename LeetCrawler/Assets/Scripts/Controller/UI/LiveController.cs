﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiveController : MonoBehaviour
{
    public Texture2D heartTexture;
    public PlayerLiveBar liveBar;
    public int maxLives = 5;

    void Start()
    {
        liveBar = new PlayerLiveBar(maxLives);
        EventManager.AddListener<PlayerHealthChanged>(ChangePlayerHealth);
    }

    void OnGUI()
    {
        // screen.width, screen.height); //Adjust the rectangle position and size for your own needs
        if (liveBar.GetCurrentHealth() > 0)
        {
            Rect r = new Rect(10, 10, heartTexture.width*liveBar.GetCurrentHealth(), heartTexture.height);
            GUILayout.BeginArea(r);
            GUILayout.BeginHorizontal();

            for (int i = 0; i < liveBar.GetCurrentHealth(); i++)
            {
                GUILayout.Label(heartTexture);
                GUILayout.FlexibleSpace();
            }

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }

    private void ChangePlayerHealth(PlayerHealthChanged e)
    {
        liveBar.ChangeHealth(e.healthChanged);

        if (liveBar.GetCurrentHealth() == 0)
        {
            EventManager.TriggerEvent(new PlayerDiedEvent());
        }
    }
}
