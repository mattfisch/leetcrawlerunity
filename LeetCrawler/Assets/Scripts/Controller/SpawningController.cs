﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningController : MonoBehaviour
{
    private float spawningHeightMin;
    private float spawningWidthMin;
    private GameObject player;

    public GameObject lootSpawnPoints;
    public List<GameObject> possibleLoot;
    public Camera mainCamera;
    public GameObject enemyContainer;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        spawningHeightMin = 2f * mainCamera.orthographicSize;
        spawningWidthMin = spawningHeightMin * mainCamera.aspect;

        EventManager.AddListener<SpawnEnemyEvent>(SpawnEnemy);
        EventManager.AddListener<SpawnLootEvent>(SpawnLoot);
        EventManager.AddListener<PlayerDiedEvent>(PlayerDied);
    }

    private void SpawnEnemy(SpawnEnemyEvent e)
    {
        for (int i = 0; i < e.amount; i++)
        {
            int pos = Random.value <= 0.5 ? 0 : 1;
            Vector2 spawnPoint = mainCamera.transform.position;

            switch (pos)
            {
                case 0:
                    {
                        spawnPoint.x += spawningWidthMin / 2 + 1;
                        break;
                    }
                case 1:
                    {
                        spawnPoint.x -= spawningWidthMin / 2 + 1;
                        break;
                    }
            }

            spawnPoint.y = player.transform.position.y;
            e.enemy.GetComponent<EnemyAIPath>().target = GameObject.FindGameObjectWithTag("Player").transform;
            e.enemy.GetComponent<EnemyBaseAI>().worth = 50;
            GameObject spawnedEnemy = Instantiate(e.enemy,
                    spawnPoint,
                    Quaternion.identity);
            spawnedEnemy.transform.parent = enemyContainer.transform;
        }
    }

    private void SpawnLoot(SpawnLootEvent e)
    {
        if (lootSpawnPoints != null)
        {
            Transform[] allChildren = lootSpawnPoints.GetComponentsInChildren<Transform>();
            Transform spawnpoint = allChildren[(int) Random.Range(0, allChildren.Length - 1)];
            GameObject loot = possibleLoot[(int) Random.Range(0, possibleLoot.Capacity - 1)];
            Instantiate(loot,
                spawnpoint.position,
                Quaternion.identity);
        }
    }

    private void PlayerDied(PlayerDiedEvent e)
    {
        foreach (Transform child in enemyContainer.transform)
        {
            child.GetComponent<EnemyAIPath>().enabled = false;
        }
    }
}
