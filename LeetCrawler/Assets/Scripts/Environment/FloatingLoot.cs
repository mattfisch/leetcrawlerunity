﻿using UnityEngine;

public class FloatingLoot : MonoBehaviour
{
    [SerializeField]
    public float upperLimit;

    private float relativeUpperLimit;
    private float lowerLimit;
    private float currentPos;
    private bool shouldIncrement;

    private void Start()
    {
        relativeUpperLimit = Mathf.Abs(transform.position.y) + upperLimit;
        lowerLimit = Mathf.Abs(transform.position.y);
        currentPos = lowerLimit;
        shouldIncrement = true;
    }

    private void FixedUpdate()
    {
        float moveSpeed = (Random.value + 0.2f) % 0.5f;
        if (shouldIncrement)
        {
            if (currentPos < relativeUpperLimit)
            {
                currentPos += Time.deltaTime * moveSpeed;
                this.transform.position += new Vector3(
                    0,
                    Time.deltaTime * moveSpeed,
                    0);
            }
            else
            {
                shouldIncrement = false;
            }
        }
        else
        {
            if (currentPos > lowerLimit)
            {
                currentPos += Time.deltaTime * moveSpeed * -1;
                this.transform.position += new Vector3(
                    0,
                    Time.deltaTime * moveSpeed * -1,
                    0);
            }
            else
            {
                shouldIncrement = true;
            }
        }
    }
}
