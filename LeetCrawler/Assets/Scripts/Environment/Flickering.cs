﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flickering : MonoBehaviour
{

    [Range(1, 100)]
    public float speed = 1f;

    [SerializeField]
    public bool enableRangeFlickering = true;

    [SerializeField]
    public float rangeMin = 1f;

    [SerializeField]
    public float rangeMax = 5f;

    [SerializeField]
    public bool enableIntesityFlickering = true;

    [SerializeField]
    public float intensityMin = 1f;

    [SerializeField]
    public float intensityMax = 5f;

    private Light FlickeringLight;

    private float rangeCounter;
    private float intensityCounter;

    private bool isRangeIncreasing;
    private bool isIntensityIncreasing;

    void Start()
    {
        FlickeringLight = GetComponent<Light>();

        intensityCounter = intensityMin;
        rangeCounter = rangeMin;

        isRangeIncreasing = true;
        isIntensityIncreasing = true;
    }

    void Update()
    {
        {
            if (enableRangeFlickering)
                RangeFlickering();

            if (enableIntesityFlickering)
                IntensityFlickering();
        }
    }

    private void RangeFlickering()
    {
        if (rangeCounter > rangeMax)
        {
            isRangeIncreasing = false;
        }
        if (rangeCounter < rangeMin)
        {
            isRangeIncreasing = true;
        }

        if (isRangeIncreasing)
            rangeCounter += 0.001f * speed;
        else
            rangeCounter -= 0.001f * speed;

        FlickeringLight.range = rangeCounter;
    }

    private void IntensityFlickering()
    {
        if (intensityCounter > intensityMax)
        {
            isIntensityIncreasing = false;
        }
        if (intensityCounter < intensityMin)
        {
            isIntensityIncreasing = true;
        }

        if (isIntensityIncreasing)
            intensityCounter += 0.01f * speed;
        else
            intensityCounter -= 0.01f * speed;

        FlickeringLight.intensity = intensityCounter;
    }
}
