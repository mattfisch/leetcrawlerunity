﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Handles all events regarding the player like checkpoints, storypoints, items.
/// </summary>
public class PlayerEvents : MonoBehaviour
{
    private void Start()
    {
        EventManager.AddListener<PlayerDiedEvent>(OnPlayerDied);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string triggeredTag = collision.transform.tag;
        if (triggeredTag.Equals("Loot"))
        {
            if (collision.transform.name.StartsWith("Health"))
            {
                EventManager.TriggerEvent(new PlayerHealthChanged(1));
            }
            EventManager.TriggerEvent(new ChangeScoreEvent(100));
            EventManager.TriggerEvent(new SpawnLootEvent());
            EventManager.TriggerEvent(new PickupLootEvent());
            Destroy(collision.gameObject);
        }

        if (triggeredTag.Equals("Finish"))
        {
            EventManager.TriggerEvent(new ChangeScoreEvent(1000));
            EventManager.TriggerEvent(new PlayerFinishedEvent());
         
            StartCoroutine(SwapScene());
        }
    }

    private void OnPlayerDied(PlayerDiedEvent e)
    {
        GetComponent<Animator>().SetTrigger("shouldDie");
        GetComponent<PlayerController>().enabled = false;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
     
        StartCoroutine(SwapScene());
    }

    private IEnumerator SwapScene()
    {
        yield return new WaitForSeconds(5);
        EventManager.TriggerEvent(new SceneChangedEvent("ScoreScreen"));
        yield return null;
    }

}
