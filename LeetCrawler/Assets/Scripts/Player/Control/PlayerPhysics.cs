﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public abstract class PlayerPhysics : MonoBehaviour
{
    public float jumpVelocity = 11.8f;
    public float moveVelocity = 7;

    public LayerMask canGroundOn;

    protected bool willJump;
    public bool isGrounded;

    protected Rigidbody2D playerRB;
    protected Transform playerTF;
    private Transform groundRaycast;
    protected Vector2 targetVelocity;

    //TO BE MOVED
    private float restartTimer;

    void Start()
    {
        playerTF = GetComponent<Transform>();
        playerRB = GetComponent<Rigidbody2D>();
        groundRaycast = GameObject.Find(name + "/GroundRaycast").transform;
    }

    void Update()
    {
        targetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    public abstract void ComputeVelocity();

    void FixedUpdate()
    {
        isGrounded = Physics2D.Linecast(playerTF.position, groundRaycast.position, canGroundOn);

        Vector2 newVelocity = playerRB.velocity;
        newVelocity.x = targetVelocity.x;

        playerRB.velocity = newVelocity;
        if (willJump)
            playerRB.velocity = jumpVelocity * Vector2.up;
    }

    public void AddKnockbackForce(Vector2 force)
    {
        playerRB.velocity = force;
    }
}
