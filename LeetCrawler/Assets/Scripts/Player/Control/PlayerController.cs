﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : PlayerPhysics
{
    public bool isBlocking = false;
    private bool willBlock = false;
    private bool isAttacking = false;

    private Collider2D weaponCollider;
    private Animator animator;

    // Use this for initialization
    void Awake()
    {
        animator = GetComponent<Animator>();
        playerTF = GetComponent<Transform>();
        weaponCollider = GameObject.FindGameObjectWithTag("PlayerWeapon").GetComponent<Collider2D>();
        weaponCollider.enabled = false;
    }

    public override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
        move.x = Input.GetAxis("Horizontal");

        willJump = Input.GetButtonDown("Jump") && isGrounded;

        if (Input.GetButtonDown("Attack") && !isAttacking)
        {
            isAttacking = true;
            weaponCollider.enabled = true;
            StartCoroutine(AttackingCoroutine());
        }

        if (Input.GetButtonDown("Block") && !willBlock)
        {
            willBlock = true;
        }

        if (Input.GetButtonUp("Block"))
        {
            willBlock = false;
            isBlocking = false;
        }

        // Mobile control
#else
        move.x = CrossPlatformInputManager.GetAxis("Horizontal");

        willJump = CrossPlatformInputManager.GetButtonDown("Jump") && isGrounded;

        if (CrossPlatformInputManager.GetButtonDown("Attack") && !isAttacking)
        {
            isAttacking = true;
            weaponCollider.enabled = true;
            StartCoroutine(AttackingCoroutine());
        }

        if (CrossPlatformInputManager.GetButtonDown("Block") && !willBlock)
        {
            willBlock = true;
        }

        if (CrossPlatformInputManager.GetButtonUp("Block"))
        {
            willBlock = false;
            isBlocking = false;
        }
#endif
        
        if (willBlock)
        {
            animator.SetBool("shouldBlock", willBlock);
            isBlocking = true;
            willBlock = false;
            animator.SetBool("shouldHoldBlock", isBlocking);
        }
        else
        {
            animator.SetBool("shouldBlock", willBlock);
            animator.SetBool("shouldHoldBlock", isBlocking);
        }

        animator.SetBool("shouldWalk", move.x != 0.0f);

        //rotate player left and right
        if (move.x < 0 && playerTF.localRotation.eulerAngles.y == 0)
        {
            playerTF.Rotate(0, 180, 0);
        }
        else if (move.x > 0 && playerTF.localRotation.eulerAngles.y >= 180)
        {
            playerTF.rotation = Quaternion.identity;
        }

        if (isBlocking)
        {
            targetVelocity = new Vector2(0.0f, 0.0f) * moveVelocity;
        }
        else
        {
            targetVelocity = move * moveVelocity;
        }
        
    }

    IEnumerator AttackingCoroutine()
    {
        animator.SetTrigger("shouldAttack");
        EventManager.TriggerEvent(new PlayerAttackEvent());
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        isAttacking = false;
        weaponCollider.enabled = false;
        yield return null;
    }
}
