﻿public class EnemyAIPath : AIPath
{
    public override void OnTargetReached()
    {
        base.OnTargetReached();
        EnemyBaseAI unit = gameObject.GetComponent<EnemyBaseAI>();
        if (unit != null)
        {
            unit.OnTargetsVincinityReached(target.gameObject);
        }
    }
}
