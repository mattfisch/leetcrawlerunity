﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIPath), typeof(Animator))]
public abstract class EnemyBaseAI : MonoBehaviour
{
    public int attackForceX = 900;
    public int attackForceY = 9;
    public int worth;

    private bool isMoving;
    private Vector3 origPos;
    protected Animator animator;
    protected AIPath aiPath;
    public Collider2D attackVincinityCollider;
    

    public abstract void DoUnitAttack(GameObject other);
    public abstract void OnTargetsVincinityReached(GameObject target);

    protected virtual void Start()
    {
        isMoving = false;
        aiPath = gameObject.GetComponent<AIPath>();
        animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        //check if AI is currently able to move and should, or not
        if (aiPath.canMove && !isMoving)
        {
            animator.SetBool("ShouldMove", true);
            isMoving = true;
        }
        else if (!aiPath.canMove && isMoving)
        {
            animator.SetBool("ShouldMove", false);
            isMoving = false;
        }

        Vector3 moveDirection = gameObject.transform.position - origPos;
        if (moveDirection != Vector3.zero)
        {
            if (moveDirection.x < 0)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
        }

        origPos = gameObject.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //AI might have multiple colliders with trigger attached, check
        //if the triggered one was the attackVincinityCollider
        if (attackVincinityCollider.IsTouching(collision))
        {
            //only attack if it collided with the player
            if (collision.gameObject.tag.Equals("Player"))
            {
                StartCoroutine(AttackCoroutine(collision.gameObject));
            }

            if (collision.gameObject.tag.Equals("PlayerWeapon"))
            {
                aiPath.canMove = false;
                EventManager.TriggerEvent(new EnemyHitEvent());
                StartCoroutine(DyingCoroutine());
            }
        }
    }

    IEnumerator AttackCoroutine(GameObject other)
    {
        //stop moving during attacking and show animation
        aiPath.canMove = false;
        animator.SetTrigger("shouldAttack");

        //check if player was already killed
        if (other.gameObject.transform.tag == "Dead")
        {
            aiPath.canSearch = false;
            aiPath.target = null;
            yield return null;
        }

        //after half the attack animation actualy perform attack logic
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length / 2);

        EventManager.TriggerEvent(new EnemyAttackEvent());

        float attackForce = 40.0f;
        float offset = gameObject.transform.position.x - other.transform.position.x;
       
        int attackDirection = (offset <= 0 ? 1 : -1);
     
        if( other.GetComponent<PlayerController>().isBlocking)
        {
            bool otherUnitIsLookingLeft = other.GetComponent<Transform>().rotation.y < 0.0f;
            bool thisUnitIsLookingLeft = gameObject.GetComponent<SpriteRenderer>().flipX;
            
            // Player facing enemy with blocking shield
            if (otherUnitIsLookingLeft != thisUnitIsLookingLeft) 
            {
                //only move a third as strong since it was blocked.
                attackForce = attackForce/3;
                EventManager.TriggerEvent(new PlayerHitEvent("BlockedHit"));
            }
            else
            {
                // Player is looking in the opposite direction full hit
                EventManager.TriggerEvent(new PlayerHitEvent("Hit"));
                EventManager.TriggerEvent(new PlayerHealthChanged(-1));
            }
        }
        else
        {
            EventManager.TriggerEvent(new PlayerHitEvent("Hit"));
            EventManager.TriggerEvent(new PlayerHealthChanged(-1));
        }
        var attack = (attackForceX * attackForce) * attackDirection;

        other.GetComponent<Rigidbody2D>().AddForce(new Vector2(attack , attackForceY));

        DoUnitAttack(other);
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length / 2);

        //start moving again and finish animation
        aiPath.canMove = true;

        yield return null;
    }

    IEnumerator DyingCoroutine()
    {
        animator.SetBool("ShouldDie", true);
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        EventManager.TriggerEvent(new EnemyDiedEvent(this.gameObject));
        yield return null;
    }
}