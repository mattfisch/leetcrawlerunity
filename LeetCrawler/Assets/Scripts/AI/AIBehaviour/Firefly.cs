﻿using UnityEngine;

public class Firefly : MonoBehaviour
{
    [SerializeField]
    public int frameCount = 10;

    [SerializeField]
    public float moveSpeed = 0.5f;

    private float xMovement;
    private float yMovement;
    private int currentFrameCount;

    private void Start()
    {
        currentFrameCount = 0;
        xMovement = 0;
        yMovement = 0;
    }

    void Update()
    {
        if (currentFrameCount == frameCount - 1)
        {
            xMovement = (Random.Range(1, 3)) * RandomSign();
            yMovement = (Random.Range(1, 3)) * RandomSign();
        }

        this.transform.position += new Vector3(
            Time.deltaTime * xMovement * moveSpeed,
            Time.deltaTime * yMovement * moveSpeed, 0);

        currentFrameCount = (++currentFrameCount % frameCount);
    }

    private int RandomSign()
    {
        return Random.value < .5 ? 1 : -1;
    }
}
